import React, { PureComponent } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import RouteWithSubRoutes from './components/common/RouteWithSubRoutes';
import routes from './routers/routes';

class App extends PureComponent {
  componentDidMount = () => {
    console.log('init app');
  };

  render() {
    return (
      <Router>
        <div>
          {routes.map(route => (
            <RouteWithSubRoutes key={JSON.stringify(route)} {...route} />
          ))}
        </div>
      </Router>
    );
  }
}

export default App;
