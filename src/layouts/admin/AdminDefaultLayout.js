import React, { PureComponent } from 'react';
import { Switch, Redirect } from 'react-router-dom';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import {
  CssBaseline,
  Drawer,
  AppBar,
  Toolbar,
  Divider,
  IconButton,
  Typography,
  Badge,
  Icon,
  TextField,
  InputAdornment,
} from '@material-ui/core';
import AdminAside from './AdminAside';
import RouteWithSubRoutes from '../../components/common/RouteWithSubRoutes';
import withRoot from './withRoot';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  drawerHeading: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  drawerHeadingText: {
    flex: 1,
    padding: '0 12px',
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  searchForm: {
    flex: 1,
  },
  searchFieldRoot: {
    backgroundColor: '#5c6bc0',
    color: '#fff',
    alignItems: 'center',
    paddingRight: '8px',
  },
  searchFieldIcon: {
    padding: '12px',
    margin: 0,
  },
  searchFieldInput: {
    padding: '12px',
    paddingLeft: 0,
  },
});

class AdminDefaultLayout extends PureComponent {
  state = {
    open: false,
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, routes } = this.props;
    const { open } = this.state;
    return (
      <Switch>
        <Redirect exact from="/admin" to={routes[0].path} />
        <React.Fragment>
          <CssBaseline />
          <div className={classes.root}>
            <AppBar
              position="absolute"
              className={classNames(classes.appBar, open && classes.appBarShift)}
            >
              <Toolbar disableGutters={!open} className={classes.toolbar}>
                <IconButton
                  color="inherit"
                  aria-label="Open drawer"
                  onClick={this.handleDrawerOpen}
                  className={classNames(classes.menuButton, open && classes.menuButtonHidden)}
                >
                  <Icon>menu</Icon>
                </IconButton>
                <TextField
                  id="search"
                  type="search"
                  InputProps={{
                    disableUnderline: true,
                    startAdornment: (
                      <InputAdornment className={classes.searchFieldIcon} position="start">
                        <Icon>search</Icon>
                      </InputAdornment>
                    ),
                    classes: {
                      root: classes.searchFieldRoot,
                      input: classes.searchFieldInput,
                    },
                  }}
                  fullWidth
                  placeholder="Search everything"
                />
                <IconButton color="inherit">
                  <Badge badgeContent={4} color="secondary">
                    <Icon>notifications</Icon>
                  </Badge>
                </IconButton>
                <IconButton color="inherit">
                  <Icon>account_circle</Icon>
                </IconButton>
              </Toolbar>
            </AppBar>
            <Drawer
              variant="permanent"
              classes={{
                paper: classNames(classes.drawerPaper, !open && classes.drawerPaperClose),
              }}
              open={open}
            >
              <div className={classes.drawerHeading}>
                <div className={classes.drawerHeadingText}>
                  <Typography variant="headline" color="inherit">
                    MyCMS
                  </Typography>
                  <Typography variant="subheading">v1.0.0</Typography>
                </div>
                <div className={classes.toolbarIcon}>
                  <IconButton onClick={this.handleDrawerClose}>
                    <Icon>chevron_left</Icon>
                  </IconButton>
                </div>
              </div>
              <Divider />
              <AdminAside />
            </Drawer>
            <main className={classes.content}>
              <div className={classes.appBarSpacer} />
              {routes.map(route => (
                <RouteWithSubRoutes key={JSON.stringify(route)} {...route} />
              ))}
            </main>
          </div>
        </React.Fragment>
      </Switch>
    );
  }
}

export default withRoot(withStyles(styles)(AdminDefaultLayout));
