import React, { PureComponent } from 'react';
import { Typography } from '@material-ui/core';

class Dashboard extends PureComponent {
  render() {
    return (
      <div>
        <Typography variant="headline" gutterBottom>
          Dashboard
        </Typography>
      </div>
    );
  }
}

export default Dashboard;
