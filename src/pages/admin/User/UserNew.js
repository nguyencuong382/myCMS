import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Typography, Button, TextField, Grid,
} from '@material-ui/core';

const styles = theme => ({
  paper: {
    padding: '16px',
  },
  headline: {
    display: 'flex',
    paddingBottom: theme.spacing.unit * 2,
    alignItems: 'center',
  },
  headlineText: {
    display: 'inline-block',
    marginRight: '8px',
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  margin: {
    padding: theme.spacing.unit,
  },
  submitBtn: {
    selfAlign: 'flex-end',
  },
});

class UserNew extends PureComponent {
  state = {
    username: '',
    password: '',
    retypePassword: '',
    email: '',
    firstName: '',
    lastName: '',
  };

  handleChange = name => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { classes, history } = this.props;
    const {
      username, password, retypePassword, email, firstName, lastName,
    } = this.state;
    return (
      <div>
        <div className={classes.headline}>
          <Typography className={classes.headlineText} variant="headline" component="h3">
            Add New User
          </Typography>
          <Button onClick={() => history.goBack()} color="primary">
            Back
          </Button>
        </div>
        <Grid container spacing={8}>
          <Grid item md={6} xs={12}>
            <form className={classes.container} noValidate autoComplete="off">
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <TextField
                    id="username"
                    value={username}
                    label="Username"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    fullWidth
                    onChange={this.handleChange('username')}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    type="password"
                    id="password"
                    value={password}
                    label="Password"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    fullWidth
                    onChange={this.handleChange('password')}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    type="retype-password"
                    id="password"
                    value={retypePassword}
                    label="Retype Password"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    fullWidth
                    onChange={this.handleChange('retypePassword')}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="email"
                    value={email}
                    label="Email"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    placeholder="example@example.com"
                    fullWidth
                    onChange={this.handleChange('email')}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    id="firstName"
                    value={firstName}
                    label="First Name"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    fullWidth
                    onChange={this.handleChange('firstName')}
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    id="lastName"
                    value={lastName}
                    label="Last Name"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    fullWidth
                    onChange={this.handleChange('lastName')}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button variant="contained" color="primary" className={classes.submitBtn}>
                    Submit
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(UserNew);
