import adminRoutes from './adminRoutes';
import homeRoutes from './homeRoutes';

const routes = [...adminRoutes, ...homeRoutes];

export default routes;
