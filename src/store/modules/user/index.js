import * as actions from './userActions';
import * as reducer from './userReducer';
import * as sagas from './userSagas';
import * as types from './userTypes';

export default {
  actions,
  reducer,
  sagas,
  types,
};
