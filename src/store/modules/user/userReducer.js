import * as types from './userTypes';

const initialState = {
  list: {
    data: null,
    error: null,
    loading: false,
  },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case types.FETCH_ALL_USER_REQUEST:
      return {
        ...state,
        list: {
          data: null,
          error: null,
          loading: true,
        },
      };
    case types.FETCH_ALL_USER_SUCCESS:
      return {
        ...state,
        list: {
          data: action.payload,
          error: null,
          loading: false,
        },
      };
    case types.FETCH_ALL_USER_ERROR:
      return {
        ...state,
        list: {
          data: null,
          error: action.payload,
          loading: false,
        },
      };
    default:
      return state;
  }
}
