import { fork, takeEvery } from 'redux-saga/effects';
import fixtureApi from '../services/fixtureApi';

// Modules
import user from './modules/user';

const api = fixtureApi;

// Watches for FETCH_ALL_BIN_REQUEST action type asynchronously
function* watcher() {
  yield takeEvery(user.types.FETCH_ALL_USER_REQUEST, user.sagas.fetchAllUserSaga, api);
}

// Here, we register our watcher saga(s) and export as a single generator
// function (startForeman) as our root Saga.
export default function* startForman() {
  yield fork(watcher);
}
